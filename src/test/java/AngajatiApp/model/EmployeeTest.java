package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeTest {

    Employee e;

    @Before
    public void setUp() throws Exception{

        Employee e1 = new Employee("Alina","Demian", "2930122023435", DidacticFunction.LECTURER, 3500.0);


    }

    @Test
    public void getLastName() {

        Employee e2 =new Employee("Cristian", "Mera", "1940323430142", DidacticFunction.CONFERENTIAR, 3500.0);
        Employee e3 = new Employee("Daniel","Albert", "1920124049685", DidacticFunction.LECTURER, 32000.5);
        assertEquals("Mera", e2.getLastName());
        assertEquals("Albert", e3.getLastName());
        assertNotEquals("Mera", e3.getLastName());
    }

    @Test
    public void setSalary() {

        Employee e4 = new Employee("Daniela", "Baciu", "2890523048532", DidacticFunction.TEACHER, 40000.9);
        e4.setSalary(5000.0);
        assertNotEquals(4000.9, e4.getSalary());
//        assertEquals(5000.0, e4.getSalary());
    }
}
package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeMockTestWBT {

    EmployeeMock repo = new EmployeeMock();

    @Test
    public void modifyEmployee1() {
        Employee e = null;

        List<Employee> employeeList = new ArrayList<>();

        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(e, DidacticFunction.TEACHER);

        List<Employee> expectedEmployeeList = new ArrayList<>();

        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee2() {
        Employee e = new Employee(5, DidacticFunction.ASISTENT);

        List<Employee> employeeList = new ArrayList<>();
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(e, DidacticFunction.TEACHER);

        List<Employee> expectedEmployeeList = new ArrayList<>();

        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee3() {
        Employee e = new Employee(5, DidacticFunction.ASISTENT);

        Employee e1 = new Employee(6, DidacticFunction.LECTURER);

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);

        // expected
        Employee ee1 = new Employee(6, DidacticFunction.LECTURER);

        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(ee1);

        // modify
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(e, DidacticFunction.TEACHER);

        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee4() {
        Employee e = new Employee(5, DidacticFunction.TEACHER);

        Employee e1 = new Employee(5, DidacticFunction.TEACHER);

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);

        // expected
        Employee ee1 = new Employee(5, DidacticFunction.TEACHER);

        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(ee1);

        // modify
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(e, DidacticFunction.TEACHER);

        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee5() {
        Employee e = new Employee(5, DidacticFunction.ASISTENT);

        Employee e1 = new Employee(6, DidacticFunction.ASISTENT);

        Employee e2 = new Employee(5, DidacticFunction.TEACHER);

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);
        employeeList.add(e2);

        // expected
        Employee ee1 = new Employee(6, DidacticFunction.ASISTENT);

        Employee ee2 = new Employee(5, DidacticFunction.TEACHER);

        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(ee1);
        expectedEmployeeList.add(ee2);

        // modify
        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(e, DidacticFunction.TEACHER);

        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee6() {
        Employee e = new Employee(5, DidacticFunction.ASISTENT);

        Employee e1 = new Employee(6, DidacticFunction.ASISTENT);

        Employee e2 = new Employee(7, DidacticFunction.CONFERENTIAR);

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);
        employeeList.add(e2);

        //expected
        Employee ee1 = new Employee(6, DidacticFunction.ASISTENT);

        Employee ee2 = new Employee(7, DidacticFunction.CONFERENTIAR);

        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(ee1);
        expectedEmployeeList.add(ee2);

        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(e, DidacticFunction.CONFERENTIAR);

        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void modifyEmployee7() {
        Employee e = new Employee(6, DidacticFunction.TEACHER);

        Employee e1 = new Employee(6, DidacticFunction.TEACHER);

        Employee e2 = new Employee(5, DidacticFunction.LECTURER);

        Employee e3 = new Employee(6, DidacticFunction.TEACHER);

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(e1);
        employeeList.add(e2);
        employeeList.add(e3);

        //expected
        Employee ee1 = new Employee(6, DidacticFunction.TEACHER);

        Employee ee2 = new Employee(5, DidacticFunction.LECTURER);

        Employee ee3 = new Employee(6, DidacticFunction.TEACHER);

        List<Employee> expectedEmployeeList = new ArrayList<>();
        expectedEmployeeList.add(ee1);
        expectedEmployeeList.add(ee2);
        expectedEmployeeList.add(ee3);

        this.repo.setEmployeeList(employeeList);
        this.repo.modifyEmployeeFunction(e, DidacticFunction.TEACHER);

        assertEquals(expectedEmployeeList, employeeList);
    }
}
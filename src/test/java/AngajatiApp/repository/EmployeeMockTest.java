package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeMockTest {

    EmployeeRepositoryInterface repositoryInterface = new EmployeeMock();

    @Test
    public void addEmployee1() {

        Employee e = new Employee("Ana", "pop", "1020343214503", DidacticFunction.LECTURER, 1500d);

        assertTrue(repositoryInterface.addEmployee(e));
    }

    @Test
    public void addEmployee2() {

        Employee e = new Employee("Raluca", "Popescu", "20134892930410", DidacticFunction.CONFERENTIAR, 1500d);

        assertFalse(repositoryInterface.addEmployee(e));
    }

    @Test
    public void addEmployee3() {

        Employee e = new Employee("Corina", "baciu", "1293857392033", DidacticFunction.CONFERENTIAR, 1400d);

        assertTrue(repositoryInterface.addEmployee(e));
    }

    @Test
    public void addEmployee4() {

        Employee e = new Employee("Maia", "crisan", "2319234560345", DidacticFunction.CONFERENTIAR, 2500d);

        assertTrue(repositoryInterface.addEmployee(e));
    }

    @Test
    public void addEmployee5() {

        Employee e = new Employee("A", "En", "2351943204", DidacticFunction.PROFESOR, 150d);

        assertFalse(repositoryInterface.addEmployee(e));
    }

    @Test
    public void addEmployee6() {

        Employee e = new Employee("Pop", "Adriana", "2210292493204", DidacticFunction.ASISTENT, 1500d);

        assertTrue(repositoryInterface.addEmployee(e));
    }

    @Test
    public void addEmployee7() {

        Employee e = new Employee("Antonescu", "Cristian", "2034853920125", DidacticFunction.TEACHER, 2000d);

        assertTrue(repositoryInterface.addEmployee(e));
    }
    @Test
    public void addEmployee8() {

        Employee e = new Employee("Cristian", "Crisan", "5943029435", DidacticFunction.CONFERENTIAR, 1540d);

        assertFalse(repositoryInterface.addEmployee(e));
    }
}